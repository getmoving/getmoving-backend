1. download firebase-secret.json from https://console.firebase.google.com/u/0/project/getmoving-5a566/settings/serviceaccounts/adminsdk -> generate key

2. copy to `/functions` and name it: `firebase-secret.json`
3. `cd functions` && `npm run serve` to run on localhost

4. to use email service:
    - create `.env` file in `/functions`
    - get the values and save it:
    
    SMTP_EMAIL=insert.email.here
    SMTP_PW=insert.pw.here
    SMTP_RECIPIENT=insert.smtp.recipient.here
    BARION_POSKEY=insert.barion.poskey.here


deploy
in `/functions` run `firebase deploy`