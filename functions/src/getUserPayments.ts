import { Response } from "express";
import { db } from "./admin";

interface Props {
  id: string;
  CompletedAt: string;
}

export const getUserPayments = async (userId: string, res: Response, getLast?: boolean) => {
    let statusNumber = 400;
    let resp;
  
    try {
      const document = await db.collection('payments').where("userId", "==", userId).get();
      const payments = document.docs.map(doc => ({ id: doc.id, ...doc.data() })) as Props[];
      const sorted = payments.sort((a, b) => {
        return new Date(b.CompletedAt).getTime() - new Date(a.CompletedAt).getTime();
      });
      statusNumber = 200;
      resp = getLast ? sorted[0] : sorted;
    } catch (error: any) {
      statusNumber = 500;
      resp = error.message;
    } finally {
      res.status(statusNumber).send(resp);
    }
  }