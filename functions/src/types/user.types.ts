import { GeneratorType, GENDER } from "../generator/generator.types";
import { PaymentType } from "./subscription.types";

export interface UserType {
    id: string;
    disabled: boolean;
    email: string;
    emailVerified: boolean;
    firstName: string;
    lastName: string;
    generator?: GeneratorType;
    planId?: string;
    gender: GENDER;
    payment?: {
      isActive: boolean;
      isCancelled: boolean;
      recurrenceId: string;
      traceId: string;
      validUntil: number;
      paymentType: PaymentType;
    }
  }