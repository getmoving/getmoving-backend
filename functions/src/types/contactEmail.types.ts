export interface ContactEmailProps {
  email: string;
  name: string;
  message: string;
}
