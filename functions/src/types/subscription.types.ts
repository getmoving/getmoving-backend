export enum PaymentType {
    MONTHLY = 'MONTHLY',
    YEARLY = 'YEARLY',
}

// in case of modify, modify also on the frontend