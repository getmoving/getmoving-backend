import { Response } from "express";
import { db } from "./admin";
import { CategoryProps } from "./generator/generator.types";

// eslint-disable-next-line no-shadow
enum COUNTING_TYPE {
  REP = 'REPITITON',
  DUR = 'DURATION',
}

interface ExercisesProps {
  id: string;
  name: string;
  videoId: string;
  categoryIds: string[];
  count: number;
  countingType: COUNTING_TYPE;
  equipmentIds: string[];
}

const getCategories = async (res: Response) => {
  let statusNumber = 400;
  let resp;
  
  const getExercises = async () => {
    const s = await db.collection('exercises').get();
    return s.docs.map((doc) => ({ id: doc.id, ...doc.data() })) as ExercisesProps[];
  }

  try {
    const snapshot = await db.collection('categories').get();
    const categories = snapshot.docs.map(doc => ({ id: doc.id, ...doc.data() })) as CategoryProps[];
    
    const exercises = await getExercises();
    resp = categories.map((category) => ({
      id: category.id,
      name: category.name,
      count: exercises.filter((e) => e.categoryIds.includes(category.id)).length,
    }))

    statusNumber = 200;
  } catch (error: any) {
    statusNumber = 500;
    resp = error.message;
  } finally {
    res.status(statusNumber).send(resp);
  }
};

export default getCategories;
