import { Response } from "express";
import { db } from "./admin";

// eslint-disable-next-line no-shadow
enum COUNTING_TYPE {
  REP = 'REPITITON',
  DUR = 'DURATION',
}

interface ExercisesProps {
  id: string;
  name: string;
  videoId: string;
  categoryIds: string[];
  count: number;
  countingType: COUNTING_TYPE;
  equipmentIds: string[];
}

interface VideosProps {
  id: string;
  title: string;
  description: string
  duration: number;
  date: string;
  url: string;
}

const searchVideos = async (res: Response, searchText = '', categoryId?: string) => {
  let statusNumber = 400;
  let resp;
  
  const getExercises = async () => {
    const s = await db.collection('exercises').get();
    return s.docs.map((doc) => ({ id: doc.id, ...doc.data() })) as ExercisesProps[];
  }

  try {
    const snapshot = await db.collection('videos').get();
    const videos = snapshot.docs.map(doc => ({ id: doc.id, ...doc.data() })) as VideosProps[];
    const filteredVideos = videos.filter(({ title: t, description: d }) => (t + ' ' + d).toLowerCase().includes(searchText.toLowerCase()));
    
    const exercises = await getExercises();
    resp = filteredVideos.map((v) => {
      const categoryIds = exercises.find((e) => e.videoId === v.id)?.categoryIds;
      return { ...v, categoryIds }
    })
    // TODO: filter category first
    if (categoryId) {
      resp = resp.filter((r) => r.categoryIds?.includes(categoryId));
    }

    statusNumber = 200;
  } catch (error: any) {
    statusNumber = 500;
    resp = error.message;
  } finally {
    res.status(statusNumber).send(resp);
  }
};

export default searchVideos;
