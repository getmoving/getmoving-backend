import { Response } from "express";
import { sendEmail } from "./services/email.service";
import { contactEmail } from './utils/emailTemplates';
import { ContactEmailProps } from "./types/contactEmail.types";

const sendContactEmail = async (props: ContactEmailProps, res: Response) => {
  if (!process.env.SMTP_RECIPIENT) return;
  let statusNumber = 400;
  let resp;
  try {
    await sendEmail(process.env.SMTP_RECIPIENT, 'Kapcsolatfelvétel', contactEmail(props));
    statusNumber = 200;
    resp = "Email sent";
  } catch (error: any) {
    statusNumber = 507;
    resp = error.message;
  } finally {
    res.status(statusNumber).send(resp);
  }
};

export default sendContactEmail;
