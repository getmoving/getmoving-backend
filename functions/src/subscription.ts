import axios from "axios";
import { Request, Response } from "express";
import { db } from "./admin";
import { PaymentType } from "./types/subscription.types";
import { UserType } from "./types/user.types";
import { BARION_API_ROOT, BASE_URL, FIREBASE_API_ROOT, PRICES } from "./utils/constants";
import { findCoupon } from "./utils/findCoupon";
import { uuid } from "./utils/uuid";

interface Props {
  uid: string;
  paymentType: PaymentType;
  initialPayment: boolean;
  coupon: string;
  recurrenceId: string;
  traceId?: string;
}

export const subscription = async ({
  uid, paymentType, initialPayment, recurrenceId, traceId, coupon: couponName,
}: Props, res?: Response) => {
  let statusNumber = 400;
  let resp;
  let price;
  const coupon = couponName ? await findCoupon(couponName) : null;
  
  // TODO: check old users with previous subscription
  if (paymentType === PaymentType.YEARLY) {
    if (coupon && coupon.isValid) {
      const float = PRICES.YEAR - (PRICES.YEAR * coupon.percent / 100);
      price = Math.floor(float / 10) * 10
    } else {
      price = PRICES.YEAR
    }
  } else if (paymentType === PaymentType.MONTHLY && initialPayment) {
    price = PRICES.FIRST_MONTH;
  } else if (paymentType === PaymentType.MONTHLY && !initialPayment) {
    price = PRICES.MONTH;
  } else {
    price = PRICES.MONTH;
  }

  const body = {
    POSKey: process.env.BARION_POSKEY,
    GuestCheckout: true,
    InitiateRecurrence: initialPayment,
    RecurrenceId: recurrenceId,
    RecurrenceType: 'MerchantInitiatedPayment',
    TraceId: traceId, 
    PaymentType: "Immediate",
    PaymentRequestId: 'payment-' + uuid(),
    CallbackUrl: `${FIREBASE_API_ROOT}/api/barion-callback`,
    FundingSources: [
      "All"
    ],
    Currency: "HUF",
    RedirectUrl: `${BASE_URL}profile/subscription`,
    Transactions: [
      {
        POSTransactionId: uuid(),
        Payee: process.env.BARION_EMAIL,
        Total: price,
        Comment: "Getmoving subscription",
        Items: [
          {
            Name: "Subscription",
            Description: paymentType,
            Quantity: 1,
            Unit: "előfizetés",
            UnitPrice: price,
            ItemTotal: 1,
          },
        ]
      }
    ]
  }

  try {
    const { data } = await axios.post(`${BARION_API_ROOT}payment/start`, body);
    await db.collection('payments').add({ userId: uid, paymentId: data.PaymentId, recurrenceId });
    statusNumber = 200;
    resp = data;
  } catch (error: any) {
    statusNumber = 500;
    resp = error.message;
  } finally {
    res?.status(statusNumber).send(resp);
  }
};

export const callback = async (req: Request, res: Response) => {
  const { data } = await axios.get(`${BARION_API_ROOT}payment/getpaymentstate`, {
    params: {
      POSKey: process.env.BARION_POSKEY,
      PaymentId: req.body.PaymentId,
    }
  });

  const document = await db.collection('payments').where("paymentId", "==", req.body.PaymentId).get();
  const { id } = document.docs[0];
  const initialData = document.docs[0].data();
  const monthly = data.Transactions[0].Items[0].Description === PaymentType.MONTHLY;
  await db.collection("payments").doc(id).update(data);
  if (data.Status === 'Succeeded') {
    await db.collection("users").doc(initialData.userId).update({
      'payment.isActive': true,
      'payment.isCancelled': false,
      'payment.recurrenceId': initialData.recurrenceId,
      ...(data.TraceId ? { 'payment.traceId': data.TraceId } : {}),  // TODO, why is it sometimes null
      'payment.validUntil': monthly ? new Date().setMonth(new Date().getMonth() + 1) : new Date().setFullYear(new Date().getFullYear() + 1),
      'payment.paymentType': monthly ? PaymentType.MONTHLY : PaymentType.YEARLY,
    });
  } else { // edge cases
    await db.collection("users").doc(initialData.userId).update({
      'payment.isActive': false,
      'payment.isCancelled': false,
      'payment.recurrenceId': initialData.recurrenceId,
      ...(data.TraceId ? { 'payment.traceId': data.TraceId } : {}),  // TODO, why is it sometimes null
      'payment.paymentType': monthly ? PaymentType.MONTHLY : PaymentType.YEARLY,
    });
  }
 
  return res.status(200).send('ok');
}

export const paymentJob = async () => {
  const data = await db.collection("users")
    .where("payment.isActive", "==", true)
    .where("payment.validUntil", "<", Date.now()).get();

  if (data.empty) return;

  const users = data.docs.map(doc => ({ id: doc.id, ...doc.data() })) as UserType[];
  
  return await Promise.all(users.map(async (user) => {
    if (user.payment?.isCancelled === true) {
      await db.collection("users").doc(user.id).update({
        'payment.isActive': false,
      });
    } else if (user.payment?.isCancelled === false) {
      try {
        await subscription({
          uid: user.id,
          initialPayment: false,
          paymentType: user.payment.paymentType,
          recurrenceId: user.payment.recurrenceId,
          traceId: user.payment.traceId,
          coupon: '',
        });
      } catch (error) {
        console.log(error);
      }
    }
  }))
}

export const getPaymentState = async (paymentId: string, res: Response) => {
  let statusNumber = 400;
  let resp;

  try {
    const document = await db.collection('payments').where("paymentId", "==", paymentId).get();
    const paymentObj = document.docs[0]?.data();
    statusNumber = 200;
    resp = paymentObj?.Status || '';
  } catch (error: any) {
    statusNumber = 500;
    resp = error.message;
  } finally {
    res.status(statusNumber).send(resp);
  }
}

export const unsubscribe = async (userId: string, res: Response) => {
  let statusNumber = 400;
  let resp;

  try {
    await db.collection("users").doc(userId).update({
      'payment.isCancelled': true,
    });
    statusNumber = 200;
    resp = 'ok';
  } catch (error: any) {
    statusNumber = 500;
    resp = error.message;
  } finally {
    res.status(statusNumber).send(resp);
  }
}

