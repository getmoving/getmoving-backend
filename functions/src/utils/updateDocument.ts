import { Response } from "express";
import { db } from "../admin";

const updateDocument = async <T>(collection: string, id: string, body: T, res: Response) => {
  let statusNumber = 400;
  let resp;
  try {
    const document = db.collection(collection).doc(id);
    await document.update(body);
    statusNumber = 200;
  } catch (error: any) {
    statusNumber = 505;
    resp = error.message;
  } finally {
    res.status(statusNumber).send(resp);
  }
};

export default updateDocument;