import { db } from "../admin";

export const getAllData = async <T>(collection: string) => {
    const snapshot = await db.collection(collection).get();
    return snapshot.docs.map(doc => ({ id: doc.id, ...doc.data() })) as unknown as T[];
}
