import { Response } from "express";
import { db } from "../admin";

const getAllDocuments = async (collection: string, res: Response) => {
  let statusNumber = 400;
  let resp;
  try {
    const snapshot = await db.collection(collection).get();
    resp = snapshot.docs.map(doc => ({ id: doc.id, ...doc.data() }));
    statusNumber = 200;
  } catch (error: any) {
    statusNumber = 505;
    resp = error.message;
  } finally {
    res.status(statusNumber).send(resp);
  }
};

export default getAllDocuments;
