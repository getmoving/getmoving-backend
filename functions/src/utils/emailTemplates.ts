import { BASE_URL } from './constants';
import { ContactEmailProps } from '../types/contactEmail.types';

// these should be html formatted
export const verifyEmail = (link: string) => 
    `<h1>Köszönjük a regisztrációját</h1>

    <h2>Az email cím megerősítéséhez kérem kattintson az alábbi linkre</h2>
    <a href=${link}>${link}</a>
    
    <br />
    <a href="${BASE_URL}/auth/login">Ezen a linken tud bejelentkezni az oldalra.</a>`;


export const contactEmail = ({ name, email, message }: ContactEmailProps) => 
    `<h1>Get moving kapcsolat</h1>

    <p>Név</p>
    <p>${name}</p>
    <br />

    <p>Email</p>
    <p>${email}</p>
    <br />

    <p>Üzenet</p>
    <p>${message}</p>`;
