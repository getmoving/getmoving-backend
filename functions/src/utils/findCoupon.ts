import { db } from "../admin";

interface Props {
  id: string;
  code: string;
  percent: number;
  isValid: boolean;
}

export const findCoupon = async (name: string): Promise<Props | null> => {
  const document = await db.collection("coupons").where("code", "==", name).get();
    const coupons = document.docs.map(doc => ({ id: doc.id, ...doc.data() })) as Props[];
    if (!coupons || !coupons.length) return null;
    return coupons[0];
}