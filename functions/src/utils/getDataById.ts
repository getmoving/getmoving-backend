import { Response } from "express";
import { db } from "../admin";

const getDataById = async (collection: string, id: string, res: Response) => {
  let statusNumber = 400;
  let resp;
  try {
    const snapshot = await db.collection(collection).doc(id).get();
    resp = snapshot.data();
    statusNumber = 200;
  } catch (error: any) {
    statusNumber = 504;
    resp = error.message;
  } finally {
    res.status(statusNumber).send(resp);
  }
};

export default getDataById;
