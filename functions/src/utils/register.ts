import { Response } from "express";
import { db, admin } from "../admin";
import { sendEmail } from "../services/email.service";
import { GENDER } from "../generator/generator.types";
import { verifyEmail } from './emailTemplates';

interface UserProps {
  email: string;
  password: string;
  firstName: string;
  lastName: string;
  gender: GENDER;
  subbed: boolean;
  address: {
    country: string,
    zipCode: string,
    city: string,
    street: string,
    number: string,
  },
}

const createFireStoreObject = async (id: string, userObj: UserProps) => {
  const document = db.collection("users").doc(id);
  await document.create({
    email: userObj.email,
    firstName: userObj.firstName,
    lastName: userObj.lastName,
    address: userObj.address,
    disabled: false,
    emailVerified: false,
  });
};

const createUser = async ({
  email, password, firstName, lastName, gender, subbed, address,
}: UserProps, res: Response) => {
  let statusNumber = 400;
  let resp;
  try {
    const userRecord = await admin.auth().createUser({
      email, displayName: `${lastName} ${firstName}`, password,
    });
    try {
      await createFireStoreObject(userRecord.uid, { email, password, firstName, lastName, gender, subbed, address });
      const link = await admin.auth().generateEmailVerificationLink(email);
      await sendEmail(email, 'Verify email', verifyEmail(link));
      statusNumber = 200;
      resp = "Register successful";
    } catch (error: any) {
      statusNumber = 501;
      resp = error.message;
    }
  } catch (error: any) {
    statusNumber = 502;
    resp = error.message;
  } finally {
    res.status(statusNumber).send(resp);
  }
};

export default createUser;
