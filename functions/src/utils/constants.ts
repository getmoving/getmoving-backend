export const IS_PROD = process.env.FUNCTIONS_EMULATOR !== "true";
export const BASE_URL = IS_PROD ? "https://getmovingofficial.hu/" : "http://localhost:5000/";
export const BARION_API_ROOT = 'https://api.barion.com/v2/';

export const PRICES = {
    FIRST_MONTH: 1290,
    MONTH: 3590,
    YEAR: 34990,
};

export const FIREBASE_API_ROOT = 'https://us-central1-getmoving-5a566.cloudfunctions.net/app';