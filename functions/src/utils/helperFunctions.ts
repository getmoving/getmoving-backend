import { db } from "../admin";
import { GeneratorType, WeekProps, GroupsProps, ExerciseProps, GroupTypesProps, BlockProps, GENDER } from "../generator/generator.types";
import { UserType } from "../types/user.types";
import { getRandomElement } from "./getRandomElement";

export const getUserObj = async (userId: string) => {
  const userRef = db.collection("users").doc(userId);
    const doc = await userRef.get();
    if (!doc.data()) {
      throw new Error('User not found!');
    }
    return doc.data() as UserType;
}

export const fetchGeneratorNode = async (uid: string): Promise<GeneratorType> => {
    const document = db.collection("users").doc(uid);
    const doc = await document.get();
    const userObj = doc.data();
    if (!userObj?.generator) {
      throw new Error("Generator node not found");
    }
    return userObj.generator;
  };

export const getCurrentWeek = async (planId: string, weekNumber: number) => {
  const planRef = db.collection("plans").doc(planId);
  const planDoc = await planRef.get();
  const planObj = planDoc.data();
  if (!planObj) {
    throw new Error("PlanObj not found!");
  };
  
  const currentWeek = planObj[`week${weekNumber}`] as WeekProps;
  return currentWeek;
};

export const getExerciseById = async (id: string): Promise<ExerciseProps> => {
  const doc = await db.collection("exercises").doc(id).get();
  const data = { id: doc.id, ...doc.data() };
  if (!data) {
    throw new Error("Exercise not found!");
  }
  return data as ExerciseProps;
};

export const trueOrFalse = () => Math.random() < 0.5;

// Checks if all array element can be found in the target array
const hasSameElement = (arr: string[], target: string[]) => target.every((v) => arr.includes(v));

export const generateExercise = async (
  level: number,
  groupKey: keyof BlockProps,
  equipments: string[],
  gender: GENDER,
  groups: GroupsProps[],
  groupTypes: GroupTypesProps[],
  atGym: boolean,
  allExercises: ExerciseProps[],
) => {
  const getGroupTypeByGender = () => {
    const female = groupTypes.find((g) => g.name === 'Női alsótest kiegészítő');
    const male = groupTypes.find((g) => g.name === 'Férfi felsőtest kiegészítő');
    if (!female || !male) {
      throw new Error('Gender group not found!');
    }
    switch (gender) {
      case GENDER.MALE:
        return male;
      case GENDER.FEMALE:
        return female;
      default:
        return trueOrFalse() ? male : female;
    }
  }

  const selectedGroupTypes = groupTypes.filter((g) => g.code.includes(groupKey));
  const groupType = groupKey === 'c2' ? getGroupTypeByGender() : getRandomElement(selectedGroupTypes);

  const group = groups.find((g) => g.groupType === groupType.id);
  if (!group) {
    throw new Error("group not found!");
  }

  const exerciseOptions = group.levels[level].exercises;

  // TODO dont fetch all exercises (revert)
  const exercises = allExercises.filter((e) => exerciseOptions.map((o) => o.value).includes(e.id))
  const filteredExercise = atGym ? exercises : exercises.filter((e) => hasSameElement(equipments, e?.equipmentIds || []));
  const exercise = getRandomElement(filteredExercise) || getRandomElement(exercises);

  if (!exercise) {
    throw new Error("exercise not generated!");
  }
  return exercise;
};
