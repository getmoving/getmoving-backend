import { Response } from "express";
import { db } from "../admin";

const uploadDocument = async <T>(collectionPath: string, body: T, res: Response) => {
  let statusNumber = 400;
  let resp;
  try {
    await db.collection(collectionPath).add(body);
    resp = 'OK';
    statusNumber = 200;
  } catch (error: any) {
    statusNumber = 500;
    resp = error.message;
  } finally {
    res.status(statusNumber).send(resp);
  }
};

export default uploadDocument;
