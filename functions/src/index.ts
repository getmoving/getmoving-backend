import * as functions from "firebase-functions";
import { Request, Response } from "express";

import addGenerator from "./generator/addGenerator";
import generateWeek from "./generator/plan";
import app from "./app";
import registerUser from "./utils/register";
import sendContactEmail from "./contactEmail";
import getAllDocuments from "./utils/getAllDocuments";
import getLimitedVideos from "./getLimitedVideos";
import updateUser from "./updateUser";
import uploadDocument from "./utils/uploadDocument";
import updateDocument from "./utils/updateDocument";
import getDataById from "./utils/getDataById";
import dayComplete from "./generator/dayComplete";
import getCurrentWeek from "./generator/getCurrentWeek";
import getRelevantItems from "./getRelevantItems";
import regenerate from "./generator/regenerate";
import searchVideos from "./searchVideos";
import getCategories from "./getCategories";
import { callback, getPaymentState, paymentJob, subscription, unsubscribe } from "./subscription";
import { uuid } from "./utils/uuid";
import { getUserPayments } from "./getUserPayments";
import { resetPlan } from "./generator/resetPlan";
import { checkCoupon } from "./checkCoupon";

// SCHEDULE

exports.scheduledFunction = functions.pubsub.schedule('0 * * * *').onRun(paymentJob);

// TEST JOB
// app.post("/api/test-payment-job", async (_: Request, res: Response) => {
//   await paymentJob();
//   res.status(200).send('OK');
//   return;
// }
// );


// CLIENT
app.post("/api/register-user", async (req: Request, res: Response) =>
  registerUser(req.body, res)
);

app.post("/api/day-complete", async (req: Request, res: Response) =>
  dayComplete(req.body, res)
);

app.post("/api/send-contact-email", async (req: Request, res: Response) =>
  sendContactEmail(req.body, res)
);

app.get("/api/get-about", async (_: Request, res: Response) =>
  getAllDocuments('about', res)
);

app.get("/api/get-landing-videos", async (_: Request, res: Response) =>
  getAllDocuments('landing-videos', res)
);

app.get("/api/get-videos/:orderBy/:startAfter", async (req: Request, res: Response) => 
  getLimitedVideos(req.params.orderBy, req.params.startAfter, res, 'videos')
);

app.get("/api/search-videos", async (req: Request, res: Response) => 
  searchVideos(res, req.query.searchText as string, req.query.categoryId as string | undefined)
);

app.get("/api/get-relevant-recipes/:recipeId", async (req: Request, res: Response) => 
  getRelevantItems('recipes', req.params.recipeId, res)
);

app.get("/api/get-user-by-id/:id", async (req: Request, res: Response) =>
  getDataById('users', req.params.id, res)
);

app.get("/api/get-recipe-by-id/:id", async (req: Request, res: Response) =>
  getDataById('recipes', req.params.id, res)
);

app.get("/api/get-plans/:id", async (req: Request, res: Response) =>
  getDataById('plans', req.params.id, res)
);

app.put("/api/update-user/:id", async (req: Request, res: Response) =>
  updateUser(req.params.id, req.body, res)
);

app.get("/api/get-equipments", async (_: Request, res: Response) =>
  getAllDocuments('equipments', res)
);

app.post("/api/add-generator-node", async ( req: Request, res: Response) => 
  addGenerator(req.body, res)
);

app.post("/api/generate-week", async ( req: Request, res: Response) => 
  generateWeek(req.body.uid, res)
);

app.post("/api/reset-plan", async ( req: Request, res: Response) => 
  resetPlan(req.body.userId, res)
);

app.get("/api/get-categories", async (_: Request, res: Response) =>
  getCategories(res)
);

app.post("/api/regenerate", async ( req: Request, res: Response) => 
  regenerate(req.body, res)
);

app.get("/api/get-current-week/:userId/:planId", async (req: Request, res: Response) =>
  getCurrentWeek(req.params.userId, req.params.planId, res)
);

// SUBSCRIPTION
app.post("/api/subscribe", async (req: Request, res: Response) => {
  const recurrenceId = 'recurrence-' + uuid();
  await subscription({
    paymentType: req.body.paymentType,
    coupon: req.body.coupon,
    uid: req.body.uid,
    initialPayment: true,
    recurrenceId,
  }, res);
  return;
});

app.post("/api/barion-callback", async (req: Request, res: Response) => 
  callback(req, res)
);

app.get("/api/check-coupon", async (req: Request, res: Response) => 
  checkCoupon(req.query.code as string, res)
);

app.get("/api/get-payment-state", async (req: Request, res: Response) => 
  getPaymentState(req.query.paymentId as string, res)
);

app.get("/api/get-user-payments", async (req: Request, res: Response) => 
  getUserPayments(req.query.userId as string, res)
);

app.get("/api/get-last-payment", async (req: Request, res: Response) => 
  getUserPayments(req.query.userId as string, res, true)
);

app.post("/api/unsubscribe", async (req: Request, res: Response) => 
  unsubscribe(req.body.userId, res)
);


// ADMIN
app.post("/admin/upload-category", async (req: Request, res: Response) =>
  uploadDocument('categories', req.body, res)
);

app.get("/admin/get-categories", async (_: Request, res: Response) =>
  getAllDocuments('categories', res)
);

app.get("/admin/get-payments", async (_: Request, res: Response) =>
  getAllDocuments('payments', res)
);

app.get("/admin/get-users", async (_: Request, res: Response) =>
  getAllDocuments('users', res)
);

app.post("/admin/upload-recipe", async (req: Request, res: Response) =>
  uploadDocument('recipes', req.body, res)
);

app.post("/admin/upload-recipe-category", async (req: Request, res: Response) =>
  uploadDocument('recipe-categories', req.body, res)
);

app.get("/admin/get-recipes", async (_: Request, res: Response) =>
  getAllDocuments('recipes', res)
);

app.get("/admin/get-recipe-categories", async (_: Request, res: Response) =>
  getAllDocuments('recipe-categories', res)
);

app.get("/admin/get-blogs", async (_: Request, res: Response) =>
  getAllDocuments('blogs', res)
);

app.get("/admin/get-blog-categories", async (_: Request, res: Response) =>
  getAllDocuments('blog-categories', res)
);

app.post("/admin/upload-blog", async (req: Request, res: Response) =>
  uploadDocument('blogs', { ...req.body, createdAt: Date.now() }, res)
);

app.post("/admin/upload-blog-category", async (req: Request, res: Response) =>
  uploadDocument('blog-categories', req.body, res)
);

app.get("/api/get-relevant-blogs/:blogId", async (req: Request, res: Response) => 
  getRelevantItems('blogs', req.params.blogId, res)
);

app.get("/api/get-blog-by-id/:id", async (req: Request, res: Response) =>
  getDataById('blogs', req.params.id, res)
);

app.post("/admin/upload-equipment", async (req: Request, res: Response) =>
  uploadDocument('equipments', req.body, res)
);

app.post("/admin/upload-coupon", async (req: Request, res: Response) =>
  uploadDocument('coupons', req.body, res)
);

app.get("/admin/get-equipments", async (_: Request, res: Response) =>
  getAllDocuments('equipments', res)
);

app.get("/admin/get-coupons", async (_: Request, res: Response) =>
  getAllDocuments('coupons', res)
);

app.post("/admin/upload-video", async (req: Request, res: Response) =>
  uploadDocument('videos', { ...req.body, date: Date.now() }, res)
);

app.get("/admin/get-videos", async (_: Request, res: Response) =>
  getAllDocuments('videos', res)
);

app.put("/admin/update-videos/:id", async (req: Request, res: Response) =>
  updateDocument('videos', req.params.id, req.body, res)
);

app.post("/admin/upload-exercise", async (req: Request, res: Response) =>
  uploadDocument('exercises', req.body, res)
);

app.get("/admin/get-exercises", async (_: Request, res: Response) =>
  getAllDocuments('exercises', res)
);

app.post("/admin/upload-group", async (req: Request, res: Response) =>
  uploadDocument('groups', req.body, res)
);

app.get("/admin/get-groups", async (_: Request, res: Response) =>
  getAllDocuments('groups', res)
);

app.put("/admin/update-group/:id", async (req: Request, res: Response) =>
  updateDocument('groups', req.params.id, req.body, res)
);

app.put("/admin/update-coupon/:id", async (req: Request, res: Response) =>
  updateDocument('coupons', req.params.id, req.body, res)
);

app.get("/admin/get-group-types", async (_: Request, res: Response) =>
  getAllDocuments('group-types', res)
);



// EXAMPLES:

// // Create
// // Post
// app.post("/api/create", async (req: Request, res: Response) => {
//   try {
//     await db
//       .collection("products")
//       .doc("/" + req.body.id + "/")
//       .create({
//         name: req.body.name,
//         description: req.body.description,
//         price: req.body.price,
//       });

//     return res.status(200).send();
//   } catch (error: any) {
//     console.log(error);
//     return res.status(500).send(error);
//   }
// });

// //Login by UID
// //Get
// app.get("/api/get-user-by-id/:uid", async (req: Request, res: Response) =>
//   getUserById(req.params.uid, res)
// );

// // Read
// // Get
// app.get("/api/read/:id", async (req: Request, res: Response) => {
//   try {
//     const document = db.collection("products").doc(req.params.id);
//     const product = await document.get();
//     const response = product.data();

//     return res.status(200).send(response);
//   } catch (error: any) {
//     console.log(error);
//     return res.status(500).send(error);
//   }
// });

// // Update
// // Put
// app.put("/api/update/:id", async (req: Request, res: Response) => {
//   try {
//     const document = db.collection("products").doc(req.params.id);
//     await document.update({
//       name: req.body.name,
//       description: req.body.description,
//       price: req.body.price,
//     });

//     return res.status(200).send();
//   } catch (error: any) {
//     console.log(error);
//     return res.status(500).send(error);
//   }
// });

// // Delete
// app.delete("/api/delete/:id", async (req: Request, res: Response) => {
//   try {
//     const document = db.collection("products").doc(req.params.id);
//     await document.delete();
//     return res.status(200).send();
//   } catch (error: any) {
//     console.log(error);
//     return res.status(500).send(error);
//   }
// });

// Export the API to Firebase Cloud Functions
exports.app = functions.https.onRequest(app);
