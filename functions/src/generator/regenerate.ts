import { Response } from "express";
import { getUserObj, getCurrentWeek, generateExercise } from "../utils/helperFunctions";
import { BlockProps, DayNumberType, ExerciseBlockProps, ExerciseProps, GroupsProps, GroupTypesProps } from "./generator.types";
import { db } from "../admin";
import { getAllData } from "../utils/getAllData";

interface Props {
    uid: string;
    block: keyof BlockProps;
    level: number;
    dayId: DayNumberType;
}

const regenerate = async ({ block, dayId, level, uid }: Props, res: Response) => {
  let statusNumber = 400;
  let resp;
  try {
    const userObj = await getUserObj(uid);
    const planId = userObj.planId;
    const weekNumber = userObj.generator?.weekNumber;
    if (!planId || weekNumber === undefined) {
      throw new Error("PlanId or weekNumber not found!");
    }
    
    const currentWeek = await getCurrentWeek(planId, weekNumber);
    const currentDay = currentWeek[dayId];
    if (!currentDay) {
      throw new Error("currentDay not found!");
    };

    const is4thWeek = weekNumber % 4 === 3;
    const getSets = (base: number) => (is4thWeek ? base + 1 : base);
    const getReps = (base: number) =>
      is4thWeek
        ? base + ((weekNumber % 4) - 1) * base * 0.25
        : base + (weekNumber % 4) * base * 0.25;

    const groups = await getAllData<GroupsProps>("groups");
    const groupTypes = await getAllData<GroupTypesProps>("group-types");
    const allExercises = await getAllData<ExerciseProps>("exercises");

    const newExercise = await generateExercise(
      level, block, userObj.generator?.equipment || [], userObj.gender, groups, groupTypes, !!userObj.generator?.atGym, allExercises
    );
    const newBlock: ExerciseBlockProps = {
        exerciseId: newExercise.id,
        level: level.toString(),
        sets: getSets(level.toString() <= "3" && ["c1", "c2", "c3"].includes(block) ? 2 : 3),
        reps: Math.round(getReps(newExercise.count)),
    }

    const planRef = db.collection("plans").doc(planId);
    await planRef.update({ [`week${weekNumber}.${dayId}`]: {
        ...currentDay,
        exercises: {
            ...currentDay.exercises,
            [block]: newBlock
        }
    } });
    
    resp = 'OK';
    statusNumber = 200;
  } catch (error: any) {
    statusNumber = 503;
    resp = error.message;
  } finally {
    res.status(statusNumber).send(resp);
  }
};

export default regenerate;