import { Response } from "express";
import { db } from "../admin";
import {
  GeneratorType,
  ExerciseProps,
  WeekProps,
  BlockProps,
  DayNumberType,
  GenerateExercisesProps,
  GENDER,
  BlockType,
  GroupsProps,
  GroupTypesProps,
} from "./generator.types";
import { fetchGeneratorNode, generateExercise, getCurrentWeek, getUserObj, trueOrFalse } from "../utils/helperFunctions";
import { getAllData } from "../utils/getAllData";

const SHORT_WARMUP_VIDEO_ID = "RNpxW2s961cvqQXlVSB3";
const LONG_WARMUP_VIDEO_ID = "QwN02F5hmnnreWlOfG8t";

const d = new Date();
d.setHours(0, 0, 0, 0);
const MIDNIGHT = d.getTime();

const incrementLevel = async (userId: string, currentLevel: string) => {
  const document = db.collection("users").doc(userId);
  await document.update({ ["generator.fitness"]: (Number(currentLevel) + 1).toString() });
};

const addWeek = async (userId: string, currentWeek: number) => {
  const document = db.collection("users").doc(userId);
  await document.update({ ["generator.weekNumber"]: currentWeek + 1 });
  return currentWeek + 1;
};

const getExerciseById = async (id: string): Promise<ExerciseProps> => {
  const ref = db.collection("exercises").doc(id);
  const doc = await ref.get();
  const data = { id: doc.id, ...doc.data() };
  if (!data) {
    throw new Error("Exercise not found!");
  }
  return data as ExerciseProps;
};

const generateExercises = async ({
  generator: { weekNumber, fitness, exerciseLength },
  day,
  planId,
}: GenerateExercisesProps) => {
  let exercises: BlockProps = {};
  const currentWeek = await getCurrentWeek(planId, weekNumber);
  const nextWeekNumber = weekNumber + 1;
  const is4thWeek = nextWeekNumber % 4 === 3;

  const getBlock = async (block: BlockType) => {
    const getSets = (base: number) => (is4thWeek ? base + 1 : base);
    const getReps = (base: number) =>
      is4thWeek
        ? base + ((nextWeekNumber % 4) - 1) * base * 0.25
        : base + (nextWeekNumber % 4) * base * 0.25;

    const exerciseId = currentWeek[day]?.exercises[block]?.exerciseId;
    if (!exerciseId) {
      throw new Error("Prev exerciseId not found!");
    }
    const exercise = await getExerciseById(exerciseId);

    return {
      exerciseId,
      level: fitness,
      reps: Math.round(getReps(exercise.count)),
      sets: getSets(
        fitness <= "3" && ["c1", "c2", "c3"].includes(block) ? 2 : 3
      ),
    };
  };

  exercises = {
    warmUp: {
      videoId: exerciseLength === "45" ? SHORT_WARMUP_VIDEO_ID : LONG_WARMUP_VIDEO_ID,
    },
    a1: await getBlock("a1"),
    a2: await getBlock("a2"),
    a3: await getBlock("a3"),
    b1: await getBlock("b1"),
    b2: await getBlock("b2"),
    b3: await getBlock("b3"),
  };

  if (exerciseLength === "75") {
    exercises.c1 = await getBlock("c1");
    exercises.c2 = await getBlock("c2");
    exercises.c3 = await getBlock("c3");
  }
  return exercises;
};

const getInitBlock = async (
    generator: GeneratorType,
    block: BlockType,
    gender: GENDER,
    groups: GroupsProps[],
    groupTypes: GroupTypesProps[],
    allExercises: ExerciseProps[],
    regenerate?: boolean,
) => {
  const exercise = await generateExercise(
    regenerate
     ? (Number(generator.fitness) > 5 ? Number(generator.fitness) - 1 : Number(generator.fitness) + 1) // try other levels if keep getting the same exercise
     : Number(generator.fitness),
    block,
    generator.equipment,
    gender,
    groups,
    groupTypes,
    generator.atGym,
    allExercises,
  );
  return {
    exerciseId: exercise.id,
    level: generator.fitness,
    reps: exercise.count,
    sets: generator.fitness <= "3" && ["c1", "c2", "c3"].includes(block) ? 2 : 3,
  };
};

const getDifferentBlock = async (
    generator: GeneratorType,
    b: BlockType,
    gender: GENDER,
    groups: GroupsProps[],
    groupTypes: GroupTypesProps[],
    allExercises: ExerciseProps[],
    week: WeekProps,
) => {
  let i = 5;
  let block = await getInitBlock(generator, b, gender, groups, groupTypes, allExercises);

  const otherExercisesInSameBlock = [
    week.day0?.exercises[b]?.exerciseId,
    week.day1?.exercises[b]?.exerciseId,
    week.day2?.exercises[b]?.exerciseId,
    week.day3?.exercises[b]?.exerciseId,
    week.day4?.exercises[b]?.exerciseId,
  ]

  while (
    otherExercisesInSameBlock.includes(block.exerciseId) && i > 0
  ) {
    block = await getInitBlock(generator, b, gender, groups, groupTypes, allExercises, i < 3)
    i -= 1;
  }
  return block;
}

const initExercises = async (
  generator: GeneratorType,
  gender: GENDER,
  groups: GroupsProps[],
  groupTypes: GroupTypesProps[],
  allExercises: ExerciseProps[],
  week: WeekProps,
) => {
  const a1 = await getDifferentBlock(generator, "a1", gender, groups, groupTypes, allExercises, week);
  const b1 = await getDifferentBlock(generator, "b1", gender, groups, groupTypes, allExercises, week);

  const exercises: BlockProps = {
    warmUp: {
      videoId: generator.exerciseLength === "45" ? SHORT_WARMUP_VIDEO_ID : LONG_WARMUP_VIDEO_ID,
    },
    a1,
    a2: await getDifferentBlock(generator, "a2", gender, groups, groupTypes, allExercises, week),
    a3: await getDifferentBlock(generator, "a3", gender, groups, groupTypes, allExercises, week),
    b1,
    b2: await getDifferentBlock(generator, "b2", gender, groups, groupTypes, allExercises, week),
    b3: await getDifferentBlock(generator, generator.exerciseLength === "75" ? "b3" : (trueOrFalse() ? "b3" : "c3"), gender, groups, groupTypes, allExercises, week),
  };

  if (generator.exerciseLength === "75") {
    let c1 = await getDifferentBlock(generator, "c1", gender, groups, groupTypes, allExercises, week);
    let i = 5;
    while (
      (c1?.exerciseId === a1?.exerciseId ||
      c1?.exerciseId === b1?.exerciseId) &&
      i > 0
    ) {
      c1 = await getDifferentBlock(generator, "c1", gender, groups, groupTypes, allExercises, week);
      i -= 1;
    }
    exercises.c1 = c1;
    exercises.c2 = await getDifferentBlock(generator, "c2", gender, groups, groupTypes, allExercises, week);
    exercises.c3 = await getDifferentBlock(generator, "c3", gender, groups, groupTypes, allExercises, week);
  }
  return exercises;
};

const generateWeek = async (uid: string, res: Response) => {
  const week: WeekProps = {
    isDone: false,
    startDate: MIDNIGHT,
  };
  let statusNumber = 400;
  let resp;
  try {
    const userObj = await getUserObj(uid);
    const planId = userObj.planId;
    const gender = userObj.gender;
    const generator = await fetchGeneratorNode(uid);
    const is4thWeek = generator.weekNumber % 4 === 3;
    const currentWeek = planId ? await getCurrentWeek(planId, generator.weekNumber) : undefined;
    const groups = await getAllData<GroupsProps>("groups");
    const groupTypes = await getAllData<GroupTypesProps>("group-types");
    const allExercises = await getAllData<ExerciseProps>("exercises");

    for (let i = 0; i < Number(generator.exerciseNumber); i++) {
      let block: BlockProps;
      if (planId && !is4thWeek) {
        block = await generateExercises({
          generator,
          day: `day${i}` as DayNumberType,
          planId,
        });
      } else {
        if (is4thWeek && currentWeek?.isDone) {
          await incrementLevel(uid, generator.fitness);
        }
        block = await initExercises(generator, gender, groups, groupTypes, allExercises, week);
      }

      week[`day${i}` as DayNumberType] = {
        isDone: false,
        dateOfCompletion: null,
        note: "",
        exercises: block,
      };
    };

    if (planId) {
      const nextWeekNumber = await addWeek(uid, generator.weekNumber);
      await db.collection("plans").doc(planId).update({ [`week${nextWeekNumber}`]: week });
    } else {
      const planRef = await db.collection("plans").add({ ["week0"]: week });
      await db.collection("users").doc(uid).update({ planId: planRef.id });
    }

    resp = "OK";
    statusNumber = 200;
  } catch (error: any) {
    statusNumber = 510;
    resp = error.message;
  } finally {
    res.status(statusNumber).send(resp);
  }
};

export default generateWeek;
