import { Response } from "express";
import { db } from "../admin";
import { GeneratorType } from "./generator.types";

const addGenerator = async ({ uid, ...body }: GeneratorType, res: Response) => {
  let statusNumber = 400;
  let resp;
  try {
    const document = db.collection("users").doc(uid);
    await document.update({ generator: { ...body, fitness: (Number(body.fitness) - 1).toString() } });
    resp = 'OK';
    statusNumber = 200;
  } catch (error: any) {
    statusNumber = 503;
    resp = error.message;
  } finally {
    res.status(statusNumber).send(resp);
  }
};

export default addGenerator;