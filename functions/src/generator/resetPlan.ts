import { Response } from "express";
import { db, admin } from "../admin";

export const resetPlan = async (userId: string, res: Response) => {
  let statusNumber = 400;
  let resp;

  try {
    await db.collection("users").doc(userId).update({
      planId: admin.firestore.FieldValue.delete(),
      generator: admin.firestore.FieldValue.delete(),
    });
    statusNumber = 200;
    resp = 'ok';
  } catch (error: any) {
    statusNumber = 500;
    resp = error.message;
  } finally {
    res.status(statusNumber).send(resp);
  }
}
