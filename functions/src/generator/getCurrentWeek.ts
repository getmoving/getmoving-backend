import { Response } from "express";
import { db } from "../admin";
import {
  WeekProps,
  DayNumberType,
  ExerciseProps,
  BlockType,
  EquipmentProps,
  VideoProps,
  CategoryProps,
} from "./generator.types";
import { getAllData } from "../utils/getAllData";
import { fetchGeneratorNode } from "../utils/helperFunctions";

const BLOCKS = ["a1", "a2", "a3", "b1", "b2", "b3", "c1", "c2", "c3"];
const DAYS = ["A", "B", "C", "D", "E"];

const getCurrentWeek = async (userId: string, planId: string, res: Response) => {
  let statusNumber = 400;
  let resp;
  try {
    const exercises = await getAllData<ExerciseProps>("exercises");
    const equipments = await getAllData<EquipmentProps>("equipments");
    const categories = await getAllData<CategoryProps>("categories");
    const snapshot = await db.collection("plans").doc(planId).get();
    const data = snapshot.data();
    if (!data) return;
    const { weekNumber } = await fetchGeneratorNode(userId);
    const currentWeek = data[`week${weekNumber}`] as WeekProps;

    await Promise.all(DAYS.map(async (_, i) => {
      const day = currentWeek[`day${i}` as DayNumberType];
      if (!day) return;
      if (day.exercises.warmUp) {
        const warmUp = (await db.collection('videos').doc(day.exercises.warmUp?.videoId).get()).data();
        day.exercises.warmUp.video = warmUp as VideoProps;
      }
      await Promise.all(BLOCKS.map(async (b) => {
        const block = day.exercises[b as BlockType];
        if (!block) return;
        block.exercise = exercises.find((e) => e.id === block.exerciseId);
        if (!block.exercise) return;
        const video = await db.collection('videos').doc(block.exercise.videoId).get();
        block.exercise.video = video.data() as VideoProps;
        block.exercise.equipments = block.exercise.equipmentIds.map((id) => equipments.find((e) => e.id === id)) as EquipmentProps[];
        block.exercise.categories = block.exercise.categoryIds.map((id) => categories.find((c) => c.id === id)) as CategoryProps[];
      }));
    }));

    resp = currentWeek;
    statusNumber = 200;
  } catch (error: any) {
    statusNumber = 504;
    resp = error.message;
  } finally {
    res.status(statusNumber).send(resp);
  }
};

export default getCurrentWeek;
