import { Response } from "express";
import { db } from "../admin";
import { DayNumberType } from "./generator.types";
import { getUserObj, getCurrentWeek } from "../utils/helperFunctions";

interface Props {
  userId: string;
  dayId: string;
  note: string;
  date: string;
}

const dayComplete = async ({ userId, dayId, note, date }: Props, res: Response) => {
  let statusNumber = 400;
  let resp;
  try {
    const userObj = await getUserObj(userId);
    const planId = userObj.planId;
    const weekNumber = userObj.generator?.weekNumber;
    if (!planId || weekNumber === undefined) {
      throw new Error("PlanId or weekNumber not found!");
    }
    const planRef = db.collection("plans").doc(planId);
    let currentWeek = await getCurrentWeek(planId, weekNumber);
    const exercises = currentWeek[dayId as DayNumberType]?.exercises;
    if (!exercises) {
      throw new Error("Exercises not found!");
    };
    
    await planRef.update({ [`week${weekNumber}.${dayId}`]: {
      note,
      isDone: true,
      dateOfCompletion: date,
      exercises,
    } });

    currentWeek = await getCurrentWeek(planId, weekNumber);

    const dayKeys = ["day0", "day1", "day2", "day3", "day4"] as DayNumberType[];
    let daysLeft = false;
    dayKeys.map((dayKey) => {
      const day = currentWeek[dayKey];
      if (day && !day.isDone) {
        daysLeft = true;
      }
    });
    
    if (!daysLeft) {
      await planRef.update({ [`week${weekNumber}`]: {
        ...currentWeek,
        isDone: true,
      } });
    }

    statusNumber = 200;
    resp = "SUCCESS";
  } catch (error: any) {
    statusNumber = 507;
    resp = error.message;
  } finally {
    res.status(statusNumber).send(resp);
  }
};

export default dayComplete;
