export interface GeneratorType {
  exerciseNumber: "2" | "3" | "4" | "5";
  fitness: "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9";
  equipment: string[];
  weekNumber: number;
  uid: string;
  atGym: boolean;
  exerciseLength: string;
}

export interface SelectOptions {
  label: string;
  value: string;
}

export interface LevelsType {
  level: number;
  name: string;
  exercises: SelectOptions[];
}

export interface GroupsProps {
  id: string;
  groupType: string;
  levels: LevelsType[];
}

export interface GroupTypesProps {
  id: string;
  name: string;
  code: string[];
}

enum COUNTING_TYPE {
  REP = "REPITITON",
  DUR = "DURATION",
}

export interface EquipmentProps {
  id: string;
  name: string;
  imageUrl: string;
}

export interface VideoProps {
  id: string;
  title: string;
  description: string
  duration: number;
  date: string;
  url: string;
  categoryIds?: string[];
}

export interface CategoryProps {
  id: string;
  name: string;
  description: string;
  imageUrl: string;
}

export interface ExerciseProps {
  id: string;
  name: string;
  videoId: string;
  video: VideoProps;
  categoryIds: string[];
  categories: CategoryProps[];
  count: number;
  countingType: COUNTING_TYPE;
  equipmentIds: string[];
  equipments?: EquipmentProps[];
}

export interface ExerciseBlockProps {
  exercise?: ExerciseProps;
  exerciseId: string;
  reps: number;
  sets: number;
  level: string;
}

export interface WarmUpProps {
  videoId: string;
  video?: VideoProps;
}

export interface BlockProps {
  warmUp?: WarmUpProps;
  a1?: ExerciseBlockProps;
  a2?: ExerciseBlockProps;
  a3?: ExerciseBlockProps;
  b1?: ExerciseBlockProps;
  b2?: ExerciseBlockProps;
  b3?: ExerciseBlockProps;
  c1?: ExerciseBlockProps;
  c2?: ExerciseBlockProps;
  c3?: ExerciseBlockProps;
}

export interface DayProps {
  isDone: boolean;
  dateOfCompletion: string | null;
  note: string;
  exercises: BlockProps;
}

export interface WeekProps {
  isDone: boolean;
  startDate: number;
  day0?: DayProps;
  day1?: DayProps;
  day2?: DayProps;
  day3?: DayProps;
  day4?: DayProps;
}

export type DayNumberType = "day0" | "day1" | "day2" | "day3" | "day4";
export type BlockType = 'a1' | 'a2' | 'a3' | 'b1' | 'b2' | 'b3' | 'c1' | 'c2' | 'c3';

export interface GenerateExercisesProps {
  generator: GeneratorType;
  day: DayNumberType;
  planId: string;
}

export enum GENDER {
  OTHER = 'other',
  MALE = 'male',
  FEMALE = 'female',
}
