import { Response } from "express";
import { db, admin } from "./admin";

interface Props {
  email: string;
  firstName: string;
  lastName: string;
}

const updateUser = async (id: string, body: Props, res: Response) => {
  let statusNumber = 400;
  let resp;
  try {
    await admin.auth()
      .updateUser(id, { email: body.email, displayName: `${body.lastName} ${body.firstName}` });
    const document = db.collection("users").doc(id);
    await document.update(body);
    resp = 'OK';
    statusNumber = 200;
  } catch (error: any) {
    statusNumber = 503;
    resp = error.message;
  } finally {
    res.status(statusNumber).send(resp);
  }
};

export default updateUser;
