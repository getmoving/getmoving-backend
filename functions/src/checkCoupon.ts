import { Response } from "express";
import { findCoupon } from "./utils/findCoupon";

export const checkCoupon = async (code: string, res: Response) => {
    let statusNumber = 400;
    let resp;
  
    try {
      const coupon = await findCoupon(code);
      statusNumber = 200;
      resp = !!coupon?.isValid;
    } catch (error: any) {
      statusNumber = 500;
      resp = error.message;
    } finally {
      res.status(statusNumber).send(resp);
    }
  }