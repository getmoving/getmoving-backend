import admin = require("firebase-admin");
import serviceAccount = require("../firebase-secret.json");

admin.initializeApp({
  credential: admin.credential.cert((serviceAccount as unknown) as string),
  databaseURL: "https://getmoving-5a566.firebaseio.com",
});

const db = admin.firestore();

export { db, admin };
