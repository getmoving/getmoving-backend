import { Response } from "express";
import { db } from "./admin";

// eslint-disable-next-line no-shadow
enum COUNTING_TYPE {
  REP = 'REPITITON',
  DUR = 'DURATION',
}

interface ExercisesProps {
  id: string;
  name: string;
  videoId: string;
  categoryIds: string[];
  count: number;
  countingType: COUNTING_TYPE;
  equipmentIds: string[];
}

interface VideosProps {
  id: string;
  title: string;
  description: string
  duration: number;
  date: string;
  url: string;
}

const getLimitedVideos = async (orderBy: string, startAfter: string, res: Response, collection: string) => {
  const limit = 20;
  let statusNumber = 400;
  let resp;
  let snapshot;

  const getExercises = async () => {
    const s = await db.collection('exercises').get();
    return s.docs.map((doc) => ({ id: doc.id, ...doc.data() })) as ExercisesProps[];
  }

  try {
    if (startAfter === 'undefined') { // because of initial state, the axios parsing it as a string
      snapshot = await db.collection(collection)
      .orderBy(orderBy)
      .limit(limit)
      .get();
    } else {
      snapshot = await db.collection(collection)
      .orderBy(orderBy)
      .startAfter(decodeURIComponent(startAfter))
      .limit(limit)
      .get();
    }

    const videos = snapshot.docs.map(doc => ({ id: doc.id, ...doc.data() })) as VideosProps[];
    const exercises = await getExercises();

    resp = videos.map((v) => {
      const categoryIds = exercises.find((e) => e.videoId === v.id)?.categoryIds;
      return { ...v, categoryIds }
    })

    statusNumber = 200;
  } catch (error: any) {
    statusNumber = 500;
    resp = error.message;
  } finally {
    res.status(statusNumber).send(resp);
  }
};

export default getLimitedVideos;
