import nodemailer = require("nodemailer");
import SMTPTransport = require("nodemailer/lib/smtp-transport");
import { IS_PROD } from "../utils/constants";

export const sendEmail = async (to: string, subject: string, html: string) => {
  // Create a SMTP transporter object
  const transporterObj: SMTPTransport.Options = IS_PROD
    ? {
        host: "smtp.gmail.com",
        port: 465,
        secure: true,
        auth: {
          user: process.env.SMTP_EMAIL,
          pass: process.env.SMTP_PW,
        },
      }
    : {
        host: "smtp.ethereal.email",
        port: 587,
        secure: false,
        auth: await nodemailer.createTestAccount(),
      };

  const transporter = nodemailer.createTransport(transporterObj);

  // Message object
  const message = {
    from: process.env.SMTP_EMAIL,
    to,
    subject,
    html,
  };

  transporter.sendMail(message, (err, info) => {
    if (err) {
      console.log("Error occurred. " + err.message);
      return false;
    }

    console.log("Message sent: %s", info.messageId);
    // Preview only available when sending through an Ethereal account
    console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
    return true;
  });
};
