//Lásd: https://www.youtube.com/watch?v=kX8by4eCyG4&ab_channel=MaksimIvanov

import express = require("express");
import cors = require("cors");
//import csrf = require("csurf");
import cookieParser = require("cookie-parser");
import helmet = require("helmet");
require('dotenv').config();

//const csrfMiddleware = csrf({ cookie: true });

const app = express();

app.use(cors({ origin: true }));
app.use(express.urlencoded({ extended: true })); 
app.use(express.json());
app.use(cookieParser());
//app.use(csrfMiddleware);
app.use(helmet());
//app.all("*", (req, res, next) => {
//  res.cookie("XSRF-TOKEN", req.csrfToken());
//  next();
//});

export default app;
