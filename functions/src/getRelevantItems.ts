import { Response } from "express";
import { db } from "./admin";

export type Props = {
  id: string;
  categoryIds: string[];
}

const getRelevantItems = async (collection: string, currentId: string, res: Response) => {
  const limit = 3;
  let statusNumber = 400;
  let resp;

  try {
    const data = (await db.collection(collection).doc(currentId).get()).data() as Props | null;
    if (!data) return;
    const snapshot = await db.collection(collection)
    .where("categoryIds", "array-contains-any", data.categoryIds.slice(0, 10)).limit(limit).get();
    const items = snapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() })) as Props[];
    resp = items.filter((r) => r.id !== currentId);
    statusNumber = 200;
  } catch (error: any) {
    statusNumber = 500;
    resp = error.message;
  } finally {
    res.status(statusNumber).send(resp);
  }
};

export default getRelevantItems;
